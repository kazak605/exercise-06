package ru.volodin.tm;

import static ru.volodin.tm.constant.TerminalConstant.ARGUMENT_ABOUT;
import static ru.volodin.tm.constant.TerminalConstant.ARGUMENT_HELP;
import static ru.volodin.tm.constant.TerminalConstant.ARGUMENT_VERSION;
import static ru.volodin.tm.constant.TerminalConstant.COMMAND_ABOUT;
import static ru.volodin.tm.constant.TerminalConstant.COMMAND_EXIT;
import static ru.volodin.tm.constant.TerminalConstant.COMMAND_HELP;
import static ru.volodin.tm.constant.TerminalConstant.COMMAND_VERSION;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		run(args);
		displayWelcome();
		process();
	}

	private static int displayAbout() {
		System.out.println("___________________________");
		System.out.println("Program name: Task Manager");
		System.out.println("Author: Volodin Artem");
		System.out.println("e-mail (work): volodin_av@nlmk.com");
		System.out.println("e-mail (personal): discordofeden@outlook.com");
		System.out.println("___________________________");
		return 0;
	}

	private static int displayHelp() {
		System.out.println("___________________________");
		System.out.println("Usage: task-manager-VERSION.jar [argument]");
		System.out.println("___________________________");
		System.out.println("where argument include: ");
		System.out.println("-version - print application version");
		System.out.println("-about - print developer info");
		System.out.println("-help - print arguments list");
		System.out.println("___________________________");
		System.out.println("where:");
		System.out.println("VERSION - program version");
		System.out.println("___________________________");
		System.out.println("Program support next commands in terminal:");
		System.out.println("version - print application version");
		System.out.println("about - print developer info");
		System.out.println("help - print arguments list");
		System.out.println("exit - exit from program");
		System.out.println("___________________________");
		return 0;
	}

	private static int displayVersion() {
		System.out.println("___________________________");
		System.out.println("Program version: 0.0.1");
		System.out.println("___________________________");
		return 0;
	}
	
	private static int displayError() {
		System.out.println("___________________________");
		System.out.println("Error! Unknown parameter. Please, start programm with argument \"-help\"");
		System.out.println("Or enter command \"help\"");
		System.out.println("___________________________");
		System.exit(-1);
		return -1;
	}

	private static int displayWelcome() {
		System.out.println("___________________________");
		System.out.println("*** Welcome to task manager ***");
		System.out.println("___________________________");
		return 0;
	}

	private static int exitFromProgram() {
		System.out.println("___________________________");
		System.out.println("Exit from program");
		System.out.println("*** Good day. Have a fun ***");
		System.out.println("___________________________");
		return 0;
	}

	private static int runCommand(final String command) {
		switch (command) {
			case COMMAND_ABOUT:
			case ARGUMENT_ABOUT:
				return displayAbout();
			case COMMAND_HELP:
			case ARGUMENT_HELP:
				return displayHelp();
			case COMMAND_VERSION:
			case ARGUMENT_VERSION:
				return displayVersion();
			case COMMAND_EXIT:
				return exitFromProgram();
			default:
				return displayError();
		}
	}

	private static void run(final String[] args) {
		if (args == null)
			return;
		if (args.length < 1)
			return;
		final String param = args[0];
		System.exit(runCommand(param));
	}

	private static int run(final String command) {
		if (command == null)
			return -1;
		if (command.isEmpty()) {
			System.out.println("");
			return 0;
		}
		return runCommand(command);
	}

	private static void process() {
		try (final Scanner scanner = new Scanner(System.in)) {
			String command = "";
			while (!COMMAND_EXIT.equals(command)) {
				System.out.print("Enter command: ");
				command = scanner.nextLine();
				run(command);
			}
		}
	}
}
