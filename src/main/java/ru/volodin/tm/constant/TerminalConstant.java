package ru.volodin.tm.constant;

public final class TerminalConstant {

    // Terminal constants for process in terminal
    public static final String COMMAND_ABOUT = "about";
    public static final String COMMAND_HELP = "help";
    public static final String COMMAND_VERSION = "version";
    public static final String COMMAND_EXIT = "exit";
    
    // Terminal constants for launch arguments
    public static final String ARGUMENT_ABOUT = "-about";
    public static final String ARGUMENT_HELP = "-help";
    public static final String ARGUMENT_VERSION = "-version";

}